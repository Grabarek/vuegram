import firebase from 'firebase'
import 'firebase/firestore'

// firebase init goes heres
const config = {
  apiKey: "AIzaSyAnjYeKai4aM7D6hw7qn31fyPtDBmcSUd8",
  authDomain: "vuegram-cb914.firebaseapp.com",
  databaseURL: "https://vuegram-cb914.firebaseio.com",
  projectId: "vuegram-cb914",
  storageBucket: "vuegram-cb914.appspot.com",
  messagingSenderId: "900319274574"
};
firebase.initializeApp(config)

// firebase utils
const db = firebase.firestore()
const auth = firebase.auth()
const storage = firebase.storage()
const currentUser = auth.currentUser

// firebase collections
const usersCollection = db.collection('users')
const postsCollection = db.collection('posts')
const commentsCollection = db.collection('comments')
const likesCollection = db.collection('likes')

export {
  db,
  storage,
  auth,
  currentUser,
  usersCollection,
  postsCollection,
  commentsCollection,
  likesCollection
}
